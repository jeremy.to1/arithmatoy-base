# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return 'S' * n + '0'


def S(n: str) -> str:
    return 'S' + n


def addition(a: str, b: str) -> str:
    return 'S' * (a.count('S') + b.count('S')) + '0'


def multiplication(a: str, b: str) -> str:
    return 'S' * (a.count('S') * b.count('S')) + '0'


def facto_ite(n: int) -> int:
    r = 1
    for i in range(2, n + 1):
        r *= i
    return r


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    a = 0
    b = 1
    for _ in range(2, n + 1):
        a, b = b, a+b
    return b


def golden_phi(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    a = 1
    b = 1
    for _ in range(n - 1):
        a, b = b, a+b
    return b / a


def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) - 1


def pow(a: float, n: int) -> float:
    return a ** n
